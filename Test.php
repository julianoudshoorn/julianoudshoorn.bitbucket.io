<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

// Token (oauth2)
function token() {
	$url_params = "grant_type=client_credentials&".
					"client_id=WVztF5CvHy2o9-zH-iQ9lA&".
					"client_secret=KHwkBvhEFySyZvODWN4tMtfSz2unRKNkC811k2ncCz4rVlPk4yQnmDDGKK4F4u0V";
	
	$curl = curl_init();
	curl_setopt_array($curl, array(
	CURLOPT_SSL_VERIFYPEER => 0,
	CURLOPT_URL => "https://api.yelp.com/oauth2/token",
	CURLOPT_POST => TRUE,
	CURLOPT_POSTFIELDS => $url_params,
	CURLOPT_RETURNTRANSFER => TRUE,
	CURLOPT_HTTPHEADER => array(
		"cache-control: no-cache",
		"content-type: application/x-www-form-urlencoded",
		),
	));
	$response = curl_exec($curl);
	$err = curl_error($curl);
	curl_close($curl);

	if(!$err){
	   $data = json_decode($response);
	   return $data->access_token;
	} else {
		echo $err;
	}
}
// Search
function search_location($term, $location){
	$url_params = "term=".$term."&".
					"location=".$location."&".
					"limit=5";
	search($url_params);
}
function search_coords($term, $lat, $lng){
	$url_params = "term=".$term."&".
					"latitude=".$lat."&longitude=".$lng."&".
					"limit=5";
	search($url_params);
}
function search($url_params) {
    $token = token();
	
	$curl = curl_init();
	
	curl_setopt_array($curl, array(
	CURLOPT_SSL_VERIFYPEER => 0,
	CURLOPT_URL => "https://api.yelp.com/v3/businesses/search?".$url_params,
    CURLOPT_HTTPHEADER => array(
		"authorization: Bearer " . $token,
		"cache-control: no-cache",
    ),
	));
	$respone = curl_exec($curl);
	$err = curl_error($curl);
	curl_close($curl);
	
	if(!$err){
	   $data = json_decode($respone);
	   return $data; // return all
	} else {
		echo $err;
	}
}
?>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="initial-scale=1.0">
<meta charset="utf-8">
<style type="text/css">
      #map {
        height: 90%;
		width: 90%;
      }
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
	  }
</style>	
</head>
<body>
<h1>Patat in Rotterdam</h1>

<div id="map"></div>
    <script>
    var map;

	function initMap() {
        var rotterdam = {lat: 51.92156, lng: 4.47319};
        map = new google.maps.Map(document.getElementById("map"), {
          center: rotterdam,
          zoom: 13
    
    	});
var iconBase = "https://maps.google.com/mapfiles/kml/shapes/";
var icons = {
 patat: {
 icon: iconBase + "marker.png"
 }
};

var features = [
{
 position: new google.maps.LatLng(51.92156, 4.47319),
 type: "patat"
}, {
 position: new google.maps.LatLng(51.92114, 4.4708),
 type: "patat"
}, {
 position: new google.maps.LatLng(51.920629, 4.4793387),
 type: "patat"
}, {
 position: new google.maps.LatLng(51.9351156, 4.4730911),
 type: "patat"
}, {
 position: new google.maps.LatLng(51.91819, 4.4776101),
 type: "patat"
}, {
 position: new google.maps.LatLng(51.92156, 4.47319),
 type: "patat"
}
];

 features.forEach(function(feature) {
  var marker = new google.maps.Marker({
  position: feature.position,
  icon: icons["patat"].icon,
  map: map
  });  
 });
}
</script>
<script async defer 
 src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD7jRIGoQzDztCLtlun6UpFvWHgnbJqz8c&callback=initMap">
</script>
<?php 
$results = search_coords("patat","51.92156", "4.47319");
echo $results; ?>


</div>

</body>
</html>